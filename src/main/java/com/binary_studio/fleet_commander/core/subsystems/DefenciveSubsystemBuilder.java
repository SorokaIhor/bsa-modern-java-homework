package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public final class DefenciveSubsystemBuilder {

	private DefenciveSubsystemImpl defenciveSubsystem;

	public static DefenciveSubsystemBuilder named(String name) {
		var builder = new DefenciveSubsystemBuilder();
		builder.defenciveSubsystem = new DefenciveSubsystemImpl();
		builder.defenciveSubsystem.setName(name);

		return builder;
	}

	public DefenciveSubsystemBuilder pg(Integer val) {
		defenciveSubsystem.setPgRequirement(PositiveInteger.of(val));
		return this;
	}

	public DefenciveSubsystemBuilder hullRegen(Integer val) {
		defenciveSubsystem.setHullRegen(PositiveInteger.of(val));
		return this;
	}

	public DefenciveSubsystemBuilder shieldRegen(Integer val) {
		defenciveSubsystem.setShieldRegen(PositiveInteger.of(val));
		return this;
	}

	public DefenciveSubsystemBuilder impactReduction(Integer val) {
		defenciveSubsystem.setImpactReduction(PositiveInteger.of(val));
		return this;
	}

	public DefenciveSubsystemBuilder capacitorUsage(Integer val) {
		defenciveSubsystem.setCapacitorUsage(PositiveInteger.of(val));
		return this;
	}

	public DefenciveSubsystemImpl construct() {
		return defenciveSubsystem;
	}

}
