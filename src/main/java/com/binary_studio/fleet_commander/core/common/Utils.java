package com.binary_studio.fleet_commander.core.common;

public class Utils {
    public static boolean isNameEmptyOrBlank(String name) {
        return name.trim().isEmpty() == true || name == null ? true : false;
    }

    public static boolean isShieldReducesDamageMoreThanBy95percent (int damage, int shield) {
        return (int) (shield * 95.0 / 100) > damage ? true : false;
    }

    public static void main(String[] args) {
        System.out.println(isShieldReducesDamageMoreThanBy95percent(5, 4));
    }
}
