package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public final class DockedShipBuilder {

	private DockedShip dockedShip;

	public static DockedShipBuilder named(String name) {
		var builder = new DockedShipBuilder();
		builder.dockedShip = new DockedShip();
		builder.dockedShip.setName(name);
		return builder;
	}

	public DockedShipBuilder pg(Integer val) {
		dockedShip.setPg(PositiveInteger.of(val));
		return this;
	}

	public DockedShipBuilder hull(Integer val) {
		dockedShip.setHullHP(PositiveInteger.of(val));
		return this;
	}

	public DockedShipBuilder shield(Integer val) {
		dockedShip.setShieldHP(PositiveInteger.of(val));
		return this;
	}

	public DockedShipBuilder capacitorRegen(Integer val) {
		dockedShip.setCapacitorRegeneration(PositiveInteger.of(val));
		return this;
	}

	public DockedShipBuilder capacitor(Integer val) {
		dockedShip.setCapacitor(PositiveInteger.of(val));
		return this;
	}

	public DockedShip construct() {
		return dockedShip;
	}

}
