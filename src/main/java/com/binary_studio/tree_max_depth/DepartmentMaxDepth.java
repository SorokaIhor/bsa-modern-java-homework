package com.binary_studio.tree_max_depth;


public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		int deepest = 0;

		if (rootDepartment == null) {
			return new Integer(0);
		}

		if (rootDepartment.subDepartments != null) {
			for (Department department : rootDepartment.subDepartments) {
				if (department != null) {
					deepest = Math.max(deepest, calculateMaxDepth(department));
				}
			}
		}
		return new Integer(deepest + 1);
	}
}
