package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.common.Utils;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (Utils.isNameEmptyOrBlank(name)) throw new IllegalArgumentException("Name should be not null and not empty");

		return   DefenciveSubsystemBuilder.named(name)
				.pg(powergridConsumption.value())
				.capacitorUsage(capacitorConsumption.value())
				.impactReduction(impactReductionPercent.value())
				.shieldRegen(shieldRegeneration.value())
				.hullRegen(hullRegeneration.value())
				.construct();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorUsage;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {

		/* calculating how much damage will be reduced */
		double damage = incomingDamage.damage.value().doubleValue();
		double reduction = impactReduction.value().doubleValue() > 95 ? 95 : impactReduction.value().doubleValue();
		double reducedDamage = damage / 100 * reduction;

		reducedDamage = incomingDamage.damage.value().intValue() - Math.floor(reducedDamage);
		incomingDamage.setDamage(PositiveInteger.of(
				new Double(reducedDamage).intValue()
		));

		if (incomingDamage.damage.value().intValue() <= 0) {
			incomingDamage.setDamage(PositiveInteger.of(1));
		}

		return incomingDamage;
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(getShieldRegen(), getHullRegen());
	}

	public PositiveInteger getImpactReduction() {
		return impactReduction;
	}

	public PositiveInteger getShieldRegen() {
		return shieldRegen;
	}

	public PositiveInteger getHullRegen() {
		return hullRegen;
	}

	public PositiveInteger getCapacitorUsage() {
		return capacitorUsage;
	}

	public PositiveInteger getPgRequirement() {
		return pgRequirement;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setImpactReduction(PositiveInteger impactReduction) {
		this.impactReduction = impactReduction;
	}

	public void setShieldRegen(PositiveInteger shieldRegen) {
		this.shieldRegen = shieldRegen;
	}

	public void setHullRegen(PositiveInteger hullRegen) {
		this.hullRegen = hullRegen;
	}

	public void setCapacitorUsage(PositiveInteger capacitorUsage) {
		this.capacitorUsage = capacitorUsage;
	}

	public void setPgRequirement(PositiveInteger pgRequirement) {
		this.pgRequirement = pgRequirement;
	}
}
