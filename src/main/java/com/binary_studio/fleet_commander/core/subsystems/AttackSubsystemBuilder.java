package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public final class AttackSubsystemBuilder {

	private AttackSubsystemImpl attackSubsystem;

	public static AttackSubsystemBuilder named(String name) {
		var builder = new AttackSubsystemBuilder();
		builder.attackSubsystem = new AttackSubsystemImpl();
		builder.attackSubsystem.setName(name);
		return builder;
	}

	public AttackSubsystemBuilder pg(Integer val) {
		this.attackSubsystem.setPgRequirement(PositiveInteger.of(val));
		return this;
	}

	public AttackSubsystemBuilder damage(Integer val) {
		attackSubsystem.setBaseDamage(PositiveInteger.of(val));
		return this;
	}

	public AttackSubsystemBuilder optimalSize(Integer val) {
		this.attackSubsystem.setOptimalSize(PositiveInteger.of(val));
		return this;
	}

	public AttackSubsystemBuilder optimalSpeed(Integer val) {
		this.attackSubsystem.setOptimalSpeed(PositiveInteger.of(val));
		return this;
	}

	public AttackSubsystemBuilder capacitorUsage(Integer val) {
		this.attackSubsystem.setCapacitorUsage(PositiveInteger.of(val));
		return this;
	}

	public AttackSubsystemImpl construct() {
		return this.attackSubsystem;
	}

}
