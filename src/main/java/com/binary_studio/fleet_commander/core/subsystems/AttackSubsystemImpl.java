package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import java.util.Optional;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.trim().isEmpty())  {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		var attackSubSystem = AttackSubsystemBuilder.named(name)
				.pg(powergridRequirments.value())
				.capacitorUsage(capacitorConsumption.value())
				.optimalSpeed(optimalSpeed.value())
				.optimalSize(optimalSize.value())
				.damage(baseDamage.value())
				.construct();

		return attackSubSystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		int sizeReductionModifier;
		int speedReductionModifier;
		PositiveInteger damage;

		int targetSize = target.getSize().value().intValue(); //target.getSize().value().intValue() <= 0 ? 1 : target.getSize().value().intValue();
		int targetSpeed = target.getCurrentSpeed().value().intValue();
		int optimalSize = getOptimalSize().value().intValue();
		int optimalSpeed = getOptimalSpeed().value().intValue();

		/* calculating sizeReductionModifier */
		if (targetSize <= optimalSize) {
		 	sizeReductionModifier = 1;
		} else {
			sizeReductionModifier = targetSize / optimalSize;
		}

		/* calculating speedReductionModifier */
		if (targetSize <= optimalSpeed) {
			speedReductionModifier = 1;
		} else {
			speedReductionModifier = optimalSpeed/ (2 * targetSpeed);
		}

		/* calculating damage */
		damage = PositiveInteger.of(
				baseDamage.value().intValue() * Integer.min(sizeReductionModifier, speedReductionModifier)
		);

		/* round damage up */
		double roundedDamageInDouble = Math.ceil(
				new Double(damage.value().intValue())
		);

		damage = PositiveInteger.of(
				(int) roundedDamageInDouble
		);

		/* if target speed is bigger than optimal speed  */
		if (targetSpeed > optimalSpeed) {
			damage = PositiveInteger.of(
					damage.value().intValue() / 2
			);
		}

		/* if target size and speed is lesser than optimal speed and size  */
		if (targetSize < optimalSize && targetSpeed < optimalSpeed) {
			damage = PositiveInteger.of(
					damage.value().intValue() / 2
			);
		}

		/* if target size is 0 and optimal size is more than zero */
		if (targetSize <= 0 && optimalSize > 0) {
			damage = PositiveInteger.of(1);
		}

		/* return empty optional if capacitorUsage is < 1 else return damage */
		if (capacitorUsage.value().intValue() < 1) {
			Optional<PositiveInteger> emptyOptional = Optional.empty();
			return emptyOptional.get();
		} else {
			return damage;
		}

	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PositiveInteger getBaseDamage() {
		return baseDamage;
	}

	public void setBaseDamage(PositiveInteger baseDamage) {
		this.baseDamage = baseDamage;
	}

	public PositiveInteger getOptimalSize() {
		return optimalSize;
	}

	public void setOptimalSize(PositiveInteger optimalSize) {
		this.optimalSize = optimalSize;
	}

	public PositiveInteger getOptimalSpeed() {
		return optimalSpeed;
	}

	public void setOptimalSpeed(PositiveInteger optimalSpeed) {
		this.optimalSpeed = optimalSpeed;
	}

	public PositiveInteger getCapacitorUsage() {
		return capacitorUsage;
	}

	public void setCapacitorUsage(PositiveInteger capacitorUsage) {
		this.capacitorUsage = capacitorUsage;
	}

	public PositiveInteger getPgRequirement() {
		return pgRequirement;
	}

	public void setPgRequirement(PositiveInteger pgRequirement) {
		this.pgRequirement = pgRequirement;
	}
}
