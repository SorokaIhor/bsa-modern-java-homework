package com.binary_studio.tree_max_depth;

import java.util.*;

public final class Department {

	public final String name;

	public final List<Department> subDepartments;

	public Department(String name) {
		this.name = name;
		this.subDepartments = new ArrayList<>();
	}

	public Department(String name, Department... departments) {
		List<Department> subDepartments1;
		this.name = name;
		subDepartments1 = null;

		if (departments != null) {
			subDepartments1 = Arrays.asList(departments);
		}

		this.subDepartments = subDepartments1;
	}

}
