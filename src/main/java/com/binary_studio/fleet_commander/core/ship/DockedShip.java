package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemBuilder;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public  class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private DefenciveSubsystemImpl defenciveSubsystem;

	private AttackSubsystemImpl attackSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate) {
		return	 DockedShipBuilder.named(name)
				.shield(shieldHP.value())
				.hull(hullHP.value())
				.pg(powergridOutput.value())
				.capacitor(capacitorAmount.value())
				.capacitorRegen(capacitorRechargeRate.value())
				.construct();
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}

		if (subsystem != null && subsystem.getPowerGridConsumption().value().intValue() > pg.value().intValue()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value().intValue() - pg.value().intValue()
			);
		}

		if (subsystem != null && subsystem.getPowerGridConsumption().value().intValue() == pg.value().intValue()){
			pg = PositiveInteger.of(
					pg.value().intValue() - subsystem.getPowerGridConsumption().value().intValue()
			);
			attackSubsystem = (AttackSubsystemImpl) subsystem;
		}

		if (subsystem != null && subsystem.getPowerGridConsumption().value().intValue() < pg.value().intValue()){
			pg = PositiveInteger.of(
					pg.value().intValue() - subsystem.getPowerGridConsumption().value().intValue()
			);
			attackSubsystem = (AttackSubsystemImpl) subsystem;
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}

		if (subsystem != null && subsystem.getPowerGridConsumption().value().intValue() > pg.value().intValue()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value().intValue() - pg.value().intValue()
			);
		}

		if (subsystem != null && subsystem.getPowerGridConsumption().value().intValue() == pg.value().intValue()){
			pg = PositiveInteger.of(
					pg.value().intValue() - subsystem.getPowerGridConsumption().value().intValue()
			);
			defenciveSubsystem = (DefenciveSubsystemImpl) subsystem;
		}

		if (subsystem != null && subsystem.getPowerGridConsumption().value().intValue() < pg.value().intValue()){
			pg = PositiveInteger.of(
					pg.value().intValue() - subsystem.getPowerGridConsumption().value().intValue()
			);
			defenciveSubsystem = (DefenciveSubsystemImpl) subsystem;
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (defenciveSubsystem == null && attackSubsystem == null) {
			throw new NotAllSubsystemsFitted(NotAllSubsystemsFitted.BOTH_MISSING_MSG);
		}
		if (defenciveSubsystem == null) {
			throw new NotAllSubsystemsFitted(NotAllSubsystemsFitted.DEFENCIVE_MISSING_MSG);
		}

		if (attackSubsystem == null) {
			throw new NotAllSubsystemsFitted(NotAllSubsystemsFitted.ATTACK_MISSING_MSG);
		} else {
			return (CombatReadyShip) this;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PositiveInteger getShieldHP() {
		return shieldHP;
	}

	public void setShieldHP(PositiveInteger shieldHP) {
		this.shieldHP = shieldHP;
	}

	public PositiveInteger getHullHP() {
		return hullHP;
	}

	public void setHullHP(PositiveInteger hullHP) {
		this.hullHP = hullHP;
	}

	public PositiveInteger getCapacitor() {
		return capacitor;
	}

	public void setCapacitor(PositiveInteger capacitor) {
		this.capacitor = capacitor;
	}

	public PositiveInteger getCapacitorRegeneration() {
		return capacitorRegeneration;
	}

	public void setCapacitorRegeneration(PositiveInteger capacitorRegeneration) {
		this.capacitorRegeneration = capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return pg;
	}

	public void setPg(PositiveInteger pg) {
		this.pg = pg;
	}

	public DefenciveSubsystem getDefenciveSubsystemImpl() {
		return defenciveSubsystem;
	}

	public void setDefenciveSubsystem(DefenciveSubsystemImpl defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
	}

	public AttackSubsystemImpl getAttackSubsystem() {
		return attackSubsystem;
	}

	public void setAttackSubsystem(AttackSubsystemImpl attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
	}

	public DefenciveSubsystemImpl getDefenciveSubsystem() {
		return defenciveSubsystem;
	}

}
