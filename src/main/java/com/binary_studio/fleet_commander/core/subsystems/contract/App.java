package com.binary_studio.fleet_commander.core.subsystems.contract;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemBuilder;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;

public class App {
    public static void main(String ... args) {
        String name = "Mock weapon";
        Integer optimalSize = 50;
        Integer optimalTargetSpeed = 150;
        Integer baseDamage = 100;

        AttackSubsystemImpl weapon = AttackSubsystemBuilder.named(name).pg(1).capacitorUsage(1).optimalSize(optimalSize)
                .optimalSpeed(optimalTargetSpeed).damage(baseDamage).construct();
        AttackableMock target = new AttackableMock("Mock target", PositiveInteger.of(optimalSize / 2),
                PositiveInteger.of(optimalTargetSpeed / 2));

        PositiveInteger attackResult = weapon.attack(target);
    }
}
